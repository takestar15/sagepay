Sagepay::Application.routes.draw do
  authenticated :user do
    root :to => 'home#index'
  end
  root :to => "home#index"
  devise_for :users
  resources :users do
  	get :delete_token
  end
  resources :shops do
  	get :purchase
  end
end