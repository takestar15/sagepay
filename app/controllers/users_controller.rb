class UsersController < ApplicationController
  before_filter :authenticate_user!

  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
  	@user = User.find(params[:id])
  end

  def delete_token
  	@user = User.find(params[:user_id])

  	if @user.token.nil? or @user.token == ""
  		redirect_to :back, alert: "Token is not generated..." and return
  	end

  	uri = URI.parse('https://test.sagepay.com/gateway/service/removetoken.vsp')
  	http = Net::HTTP.new(uri.host, uri.port)
  	http.use_ssl = true
  	http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		request_hash = Hash.new
		request_hash["VPSProtocol"]			= "3.0"
		request_hash["TxType"]					= "REMOVETOKEN"
		request_hash["Vendor"] 					= "tonestest"
		request_hash["Token"] 					= @user.token

		request_array = Array.new
		request_hash.each { |key, value|
			request_array.push key + "=" + value
		}

		begin
			response = http.post(uri.path, request_array.join('&'))
		rescue Timeout::Error => e
			redirect_to :back, alert: "Connection timed out..." and return
		end

		response_str = response.body

		response_hash = Hash.new
		response_str.split(/[\r\n]+/).each {|entry|
			line = entry.split('=');
			response_hash[line[0]] = line[1];
		}

		# Store token
		if response_hash["Status"] == "OK" && @user.update_attributes({token: ""})
			redirect_to :back, notice: "Token is successfully removed..." and return
		else
			redirect_to :back, alert: response_str and return
		end

		redirect_to :back, notice: "Unknown error..."
  end

  def update
  	@user = User.find(params[:id])

  	# Create token
  	uri = URI.parse('https://test.sagepay.com/gateway/service/directtoken.vsp')
  	http = Net::HTTP.new(uri.host, uri.port)
  	http.use_ssl = true
  	http.verify_mode = OpenSSL::SSL::VERIFY_NONE
  	
		request_hash = Hash.new
		request_hash["VPSProtocol"]			= "3.0"
		request_hash["TxType"]					= "TOKEN"
		request_hash["Vendor"] 					= "tonestest"
		request_hash["Currency"] 				= params[:currency]
		request_hash["CardHolder"] 			= params[:card_holder_name]
		request_hash["CardNumber"] 			= params[:card_number]
		request_hash["ExpiryDate"] 			= params[:expiry_date]
		request_hash["CV2"] 						= params[:cv2]
		request_hash["CardType"] 				= params[:card_type]

		request_array = generate_direct_param request_hash

		begin
			response = http.post(uri.path, request_array.join('&'))
		rescue Timeout::Error => e
			redirect_to :back, alert: "Connection timed out..." and return
		end

		response_str = response.body

		response_hash = Hash.new
		response_str.split(/[\r\n]+/).each {|entry|
			line = entry.split('=');
			response_hash[line[0]] = line[1];
		}

		# Store token
		if response_hash["Status"] == "OK" && @user.update_attributes({token: response_hash["Token"]})
			redirect_to shops_path, notice: "You can buy the items" and return
		else
			redirect_to :back, alert: response_str and return
		end

		redirect_to :back, notice: "Unknown error..."
  end

end