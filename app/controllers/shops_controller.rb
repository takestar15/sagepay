class ShopsController < ApplicationController
  def index
  end

  def purchase
  	@user = User.find(current_user.id)
  	if @user.token.nil? or @user.token == ""
  		redirect_to edit_user_path(@user.id), alert: "You need to input the payment information for one-time payment." and return
  	end

  	# Create token
  	uri = URI.parse('https://test.sagepay.com/gateway/service/vspdirect-register.vsp')
  	http = Net::HTTP.new(uri.host, uri.port)
  	http.use_ssl = true
  	http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		request = Net::HTTP::Post.new(uri.path)
		request_hash = Hash.new
		request_hash["VPSProtocol"]					= "3.0"
		request_hash["TxType"]							= "PAYMENT"
		request_hash["Vendor"] 							= "tonestest"
		request_hash["VendorTxCode"]				= "tonestest57113.690.870440" # increase next time, like this 870441
		request_hash["Amount"]							= "499.99"
		request_hash["Currency"]						= "GBP"
		request_hash["Token"] 							= @user.token
		request_hash["CV2"] 								= "123"
		request_hash["StoreToken"] 					= "1"
		request_hash["Description"] 				= "Personal Compter"
		request_hash["DeliveryFirstnames"] 	= "Jeff"
		request_hash["DeliverySurname"] 		= "Smith"
		request_hash["DeliveryAddress1"] 		= "1 Billing Pl"
		request_hash["DeliveryCity"] 				= "London"
		request_hash["DeliveryCountry"] 		= "GB"
		request_hash["DeliveryPostCode"] 		= "B12 3PC"
		request_hash["BillingSurname"] 			= "Smith" # need to input user info by first name and last name
		request_hash["BillingFirstnames"] 	= "Jeff"
		request_hash["BillingAddress1"] 		= "1 Billing Pl" # need to input user address
		request_hash["BillingCity"] 				= "London"
		request_hash["BillingPostcode"] 		= "B12 3PC"
		request_hash["BillingCountry"] 			= "GB"
		request_hash["Apply3DSecure"] 			= "2"

		request_array = Array.new
		request_hash.each { |key, value|
			request_array.push key + "=" + value
		}

		request.body = request_array.join('&')
		response = http.request(request)

		response_str = response.body

  	redirect_to :back, notice: response_str
  end
end