class ApplicationController < ActionController::Base
  protect_from_forgery

  def get_register_token_service_uri mode
  	if mode == "test"
  		"https://test.sagepay.com/gateway/service/directtoken.vsp"
  	elsif mode == "live"
  		"https://live.sagepay.com/gateway/service/directtoken.vsp"
  	end
  end

  def get_remove_token_service_uri mode
  	if mode == "test"
  		"https://test.sagepay.com/gateway/service/removetoken.vsp"
  	elsif mode == "live"
  		"https://live.sagepay.com/gateway/service/removetoken.vsp"
  	end
  end

  def get_direct_payment_service_uri mode
    if mode == "test"
      "https://test.sagepay.com/gateway/service/vspdirect-register.vsp"
    elsif mode == "live"
      "https://live.sagepay.com/gateway/service/vspdirect-register.vsp"
    end
  end

  def parse_response response_str
  	response_hash = Hash.new
		response_str.split(/[\r\n]+/).each {|entry|
			line = entry.split('=');
			response_hash[line[0]] = line[1];
		}
		response_hash
  end

  def generate_vendor_tx_code vendor
    vendor + "." + SecureRandom.hex(10)
  end

  def generate_direct_param request_hash
  	request_array = Array.new
		request_hash.each { |key, value|
			request_array.push key + "=" + value
		}
		request_array
  end
end
